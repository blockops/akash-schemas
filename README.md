# Akash SDL Schema
This is an unofficial Akash SDL schema for use with Akash deployments.  The schema uses the [json-schema standard](http://json-schema.org) which can be utilized in many languages and tools.  

You can use it to help write Akash deployment files in vscode or for validation in other tooling and programming languages.  

![](./images/services.png)

## Why 
During my first experience with an Akash deployment I left out a single character in the deploy.yaml file.  Despite a valid yaml document, weird errors then ensued with the deployment.  The bad yaml file highlighted a bug in Akash-analytics which took me several hours to figure out.  A proper schema validator would have prevented this. My hopes are to save you that frustration and time.

Additionally, as time goes on new versions of the SDL will modify the language and having a versioned schema will help us understand why validation is failing.   

Example: changes from mainnet to testnest might have differences in the SDL 

## Usage
While there are many ways to utilize a json schema file for validation purposes.  The easiest way is to tell vscode where to find the schema.  You can edit your vscode's settings file and add the following schema.  You may need the [yaml extension](https://marketplace.visualstudio.com/items?itemName=redhat.vscode-yaml) if you don't already have it.

```json
 "yaml.schemas": {
    "https://gitlab.com/blockops/akash-schemas/raw/main/schema.json": "deploy.yaml",
    "file:///toc.schema.json": "/toc\\.yml/i"
  },
```

Once you add the schema to your schema map, you will notice that all deploy.yaml files 
now provide feedback as you type.

The deploy.yaml file will toggle between green/red when the SDL is not valid with the schema.

For required entires, defaults will be supplied which help write the SDL. 

## Development
For local development of this schema you can set the schema map to use a local file instead

```json
 "yaml.schemas": {
    "file:///Users/user1/Akash/jsonschema/schema.json": "deploy.yaml",
    "file:///toc.schema.json": "/toc\\.yml/i"
  },
```

## Notes
This is a very basic schema with constraints gleaned off the official [Akash documentation](https://docs.akash.network/intro-to-akash/stack-definition-language).  Please note the SDL language is not fully documented so there are things missing from the schema.  Additionally, for values that can be multiple types I have not supported both types yet. 


